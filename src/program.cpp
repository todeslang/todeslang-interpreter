#include <iostream>

#include "todes.h"


using namespace std;
using namespace todes;


void Program::dump_instructions () const
{
	for (unsigned int cn = 0; cn < instructions.size (); cn ++) {
		cout
			<< cn << " "
			<< instructions.at (cn)->format ()
			<< endl;
	}
}


void Program::put_bootstrap ()
{
	instructions.push_back (make_shared <CreateParameterStore> ());
	instructions.push_back (make_shared <LoadValue> (
		make_shared <StringValue> (string {"main"})
	));
	instructions.push_back (make_shared <StoreParameter> (string {"action"}));
	put_argument_resolution_instructions ();
	instructions.push_back (make_shared <Exit> ());
}


void Program::put_argument_resolution_instructions ()
{
	unsigned int create_argument_resolution_environment = instructions.size ();
	instructions.push_back (make_shared <CreateArgumentResolutionEnvironment> ());

	dynamic_pointer_cast <CreateArgumentResolutionEnvironment>
		(instructions.at (create_argument_resolution_environment))
		->start_point_of_argument_resolution_step = instructions.size ();

	instructions.push_back (make_shared <TryDirectives> ());
	instructions.push_back (make_shared <TryCapsule> ());
	instructions.push_back (make_shared <TryContinuation> ());
	instructions.push_back (make_shared <TryTerminalContinuation> ());
	instructions.push_back (make_shared <TryPrimitive> ());
	instructions.push_back (make_shared <FinalFallback> ());

	dynamic_pointer_cast <CreateArgumentResolutionEnvironment>
		(instructions.at (create_argument_resolution_environment))
		->end_point_of_argument_resolution = instructions.size ();

	instructions.push_back (make_shared <DeleteArgumentResolutionEnvironment> ());
}


void Program::compile (NodeProgram a_node_program)
{
	put_bootstrap ();

	for (auto node_directive: a_node_program.directives) {
		compile (node_directive);
	}
}


void Program::compile (NodeDirective a_node_directive)
{
	string name = a_node_directive.name;
	auto parameters = a_node_directive.parameters;
	
	if (name == string {"trans"}
		|| name == string {"force"}
		|| name == string {"suggest"}
	) {
		directives.push_back (instructions.size ());
		compile_trans_directive (parameters);
	} else if (name == string {"continuation"}) {
		directives.push_back (instructions.size ());
		compile_continuation_directive (parameters);
	} else if (name == string {"anchor"}) {
		directives.push_back (instructions.size ());
		compile_anchor_directive (parameters);
	} else {
		cerr << "Unknown directive: " << name << endl;
		throw (InvalidProgramException {});
	}
}


void Program::compile_trans_directive
	(map <string, shared_ptr <class Expression>> a_parameters)
{
	unsigned int directive_start_point = instructions.size ();

	/* anchor */
	unsigned int jump_if_lower_than_anchor = instructions.size ();
	instructions.push_back (make_shared <JumpIfLowerThanAnchor> ());
	dynamic_pointer_cast <JumpIfLowerThanAnchor>
		(instructions.back ())->directive_start_point = directive_start_point;

	/* hierarchy */
	instructions.push_back (make_shared <LoadValue> (
		make_shared <NumberValue> (0)
	));

	instructions.push_back (make_shared <PushHierarchy> ());

	if (a_parameters.find (string {"hierarchy"}) == a_parameters.end ()) {
		instructions.push_back (make_shared <LoadValue> (
			make_shared <NumberValue> (0x80)
		));
	} else {
		compile_expression (a_parameters.at (string {"hierarchy"}));
	}

	instructions.push_back (make_shared <PopHierarchy> ());

	unsigned int jump_if_bigger_than_hierarchy = instructions.size ();
	instructions.push_back (make_shared <JumpIfBiggerThanHierarchy> ());

	instructions.push_back (make_shared <PushHierarchy> ());

	/* if */
	compile_expression (a_parameters.at (string {"if"}));
	
	unsigned int jump_if_true = instructions.size ();
	instructions.push_back (make_shared <JumpIfTrue> ());
	
	instructions.push_back (make_shared <PopHierarchy> ());

	unsigned int jump = instructions.size ();
	instructions.push_back (make_shared <Jump> ());

	dynamic_pointer_cast <JumpIfTrue>
		(instructions.at (jump_if_true))->go_to = instructions.size ();
	
	instructions.push_back (make_shared <PopHierarchy> ());

	/* parameters */
	if (a_parameters.find (string {"parameters"}) != a_parameters.end ()) {
		shared_ptr <Expression> parameters_expression = a_parameters.at (string {"parameters"});
		shared_ptr <ExpressionCompound> parameters_expression_compound
			= dynamic_pointer_cast <ExpressionCompound> (parameters_expression);
		if (! parameters_expression_compound) {
			cerr << "parameters must have compound expression." << endl;
			throw (InvalidProgramException ());
		}
		auto parameters = parameters_expression_compound->parameters;
		for (auto parameter: parameters) {
			compile_expression (parameter.second);
			instructions.push_back
				(make_shared <UpdateArgumentResolutionParameter> (parameter.first));
		}
	}

	/* perform */
	if (a_parameters.find (string {"perform"}) != a_parameters.end ()) {
		cerr << "perform parameter is obsolute." << endl;
		throw (InvalidProgramException {});
	}

	instructions.push_back (make_shared <UpdateParameterGeneration> ());

	instructions.push_back (make_shared <RestartArgumentResolution> ());
	
	dynamic_pointer_cast <JumpIfLowerThanAnchor>
		(instructions.at (jump_if_lower_than_anchor))->go_to = instructions.size ();
	dynamic_pointer_cast <JumpIfBiggerThanHierarchy>
		(instructions.at (jump_if_bigger_than_hierarchy))->go_to = instructions.size ();
	dynamic_pointer_cast <Jump>
		(instructions.at (jump))->go_to = instructions.size ();
	instructions.push_back (make_shared <TryNextDirective> ());
}


void Program::compile_continuation_directive
	(map <string, shared_ptr <class Expression>> a_parameters)
{
	unsigned int directive_start_point = instructions.size ();

	/* anchor */
	unsigned int jump_if_lower_than_anchor = instructions.size ();
	instructions.push_back (make_shared <JumpIfLowerThanAnchor> ());
	dynamic_pointer_cast <JumpIfLowerThanAnchor>
		(instructions.back ())->directive_start_point = directive_start_point;

	/* hierarchy */
	instructions.push_back (make_shared <LoadValue> (
		make_shared <NumberValue> (0)
	));

	instructions.push_back (make_shared <PushHierarchy> ());

	if (a_parameters.find (string {"hierarchy"}) == a_parameters.end ()) {
		instructions.push_back (make_shared <LoadValue> (
			make_shared <NumberValue> (0x80)
		));
	} else {
		compile_expression (a_parameters.at (string {"hierarchy"}));
	}

	instructions.push_back (make_shared <PopHierarchy> ());

	unsigned int jump_if_bigger_than_hierarchy = instructions.size ();
	instructions.push_back (make_shared <JumpIfBiggerThanHierarchy> ());

	instructions.push_back (make_shared <PushHierarchy> ());

	/* if */
	compile_expression (a_parameters.at (string {"if"}));
	
	unsigned int jump_if_true = instructions.size ();
	instructions.push_back (make_shared <JumpIfTrue> ());
	
	instructions.push_back (make_shared <PopHierarchy> ());

	unsigned int jump = instructions.size ();
	instructions.push_back (make_shared <Jump> ());

	dynamic_pointer_cast <JumpIfTrue>
		(instructions.at (jump_if_true))->go_to = instructions.size ();
	
	/* jump */
	{
		if (a_parameters.find (string {"jump"}) == a_parameters.end ()) {
			cerr << "continuation directive must have jump parameter." << endl;
			throw (InvalidProgramException {});
		}
		shared_ptr <Expression> jump_expression = a_parameters.at (string {"jump"});
		shared_ptr <ExpressionCompound> jump_expression_compound
			= dynamic_pointer_cast <ExpressionCompound> (jump_expression);
		if (! jump_expression_compound) {
			cerr << "jump must have compound expression." << endl;
			throw (InvalidProgramException ());
		}
		auto parameters = jump_expression_compound->parameters;
		for (auto parameter: parameters) {
			compile_expression (parameter.second);
			instructions.push_back
				(make_shared <UpdateArgumentResolutionParameter> (parameter.first));
		}
	}

	unsigned int create_continuation = instructions.size ();
	instructions.push_back (make_shared <CreateContinuation> ());

	instructions.push_back (make_shared <RestoreParameterGeneration> ());

	/* walk */
	{
		if (a_parameters.find (string {"walk"}) == a_parameters.end ()) {
			cerr << "continuation directive must have walk parameter." << endl;
			throw (InvalidProgramException {});
		}
		shared_ptr <Expression> walk_expression = a_parameters.at (string {"walk"});
		shared_ptr <ExpressionCompound> walk_expression_compound
			= dynamic_pointer_cast <ExpressionCompound> (walk_expression);
		if (! walk_expression_compound) {
			cerr << "walk must have compound expression." << endl;
			throw (InvalidProgramException ());
		}
		auto parameters = walk_expression_compound->parameters;
		for (auto parameter: parameters) {
			compile_expression (parameter.second);
			instructions.push_back
				(make_shared <UpdateArgumentResolutionParameter> (parameter.first));
		}
	}

	dynamic_pointer_cast <CreateContinuation> (instructions.at (create_continuation))->go_to
		= instructions.size ();

	instructions.push_back (make_shared <UpdateParameterGeneration> ());

	instructions.push_back (make_shared <PopHierarchy> ());

	instructions.push_back (make_shared <RestartArgumentResolution> ());
	
	dynamic_pointer_cast <JumpIfLowerThanAnchor>
		(instructions.at (jump_if_lower_than_anchor))->go_to = instructions.size ();
	dynamic_pointer_cast <JumpIfBiggerThanHierarchy>
		(instructions.at (jump_if_bigger_than_hierarchy))->go_to = instructions.size ();
	dynamic_pointer_cast <Jump>
		(instructions.at (jump))->go_to = instructions.size ();
	instructions.push_back (make_shared <TryNextDirective> ());
}


void Program::compile_anchor_directive
	(map <string, shared_ptr <class Expression>> a_parameters)
{
	unsigned int directive_start_point = instructions.size ();

	/* anchor */
	unsigned int jump_if_lower_than_anchor = instructions.size ();
	instructions.push_back (make_shared <JumpIfLowerThanAnchor> ());
	dynamic_pointer_cast <JumpIfLowerThanAnchor>
		(instructions.back ())->directive_start_point = directive_start_point;

	/* hierarchy */
	instructions.push_back (make_shared <LoadValue> (
		make_shared <NumberValue> (0)
	));

	instructions.push_back (make_shared <PushHierarchy> ());

	if (a_parameters.find (string {"hierarchy"}) == a_parameters.end ()) {
		instructions.push_back (make_shared <LoadValue> (
			make_shared <NumberValue> (0x80)
		));
	} else {
		compile_expression (a_parameters.at (string {"hierarchy"}));
	}

	instructions.push_back (make_shared <PopHierarchy> ());

	unsigned int jump_if_bigger_than_hierarchy = instructions.size ();
	instructions.push_back (make_shared <JumpIfBiggerThanHierarchy> ());

	instructions.push_back (make_shared <PushHierarchy> ());

	/* if */
	compile_expression (a_parameters.at (string {"if"}));
	
	unsigned int jump_if_true = instructions.size ();
	instructions.push_back (make_shared <JumpIfTrue> ());
	
	instructions.push_back (make_shared <PopHierarchy> ());

	unsigned int jump = instructions.size ();
	instructions.push_back (make_shared <Jump> ());

	dynamic_pointer_cast <JumpIfTrue>
		(instructions.at (jump_if_true))->go_to = instructions.size ();
	
	/* parameters */
	if (a_parameters.find (string {"parameters"}) != a_parameters.end ()) {
		shared_ptr <Expression> parameters_expression = a_parameters.at (string {"parameters"});
		shared_ptr <ExpressionCompound> parameters_expression_compound
			= dynamic_pointer_cast <ExpressionCompound> (parameters_expression);
		if (! parameters_expression_compound) {
			cerr << "parameters must have compound expression." << endl;
			throw (InvalidProgramException ());
		}
		auto parameters = parameters_expression_compound->parameters;
		for (auto parameter: parameters) {
			compile_expression (parameter.second);
			instructions.push_back
				(make_shared <UpdateArgumentResolutionParameter> (parameter.first));
		}
	}

	instructions.push_back (make_shared <PopHierarchy> ());

	instructions.push_back (make_shared <UpdateParameterGeneration> ());

	instructions.push_back (make_shared <SetAnchor> ());
	dynamic_pointer_cast <SetAnchor> (instructions.back ())->anchor
		= directive_start_point;

	instructions.push_back (make_shared <RestartArgumentResolution> ());
	
	dynamic_pointer_cast <JumpIfLowerThanAnchor>
		(instructions.at (jump_if_lower_than_anchor))->go_to = instructions.size ();
	dynamic_pointer_cast <JumpIfBiggerThanHierarchy>
		(instructions.at (jump_if_bigger_than_hierarchy))->go_to = instructions.size ();
	dynamic_pointer_cast <Jump>
		(instructions.at (jump))->go_to = instructions.size ();
	instructions.push_back (make_shared <TryNextDirective> ());
}


void Program::compile_expression (shared_ptr <Expression> a_expression)
{
	auto expression_string = dynamic_pointer_cast <ExpressionString> (a_expression);
	auto expression_special = dynamic_pointer_cast <ExpressionSpecial> (a_expression);
	auto expression_parameter = dynamic_pointer_cast <ExpressionParameter> (a_expression);
	auto expression_compound = dynamic_pointer_cast <ExpressionCompound> (a_expression);

	if (expression_string) {
		compile_expression_string (expression_string);
	} else if (expression_special) {
		compile_expression_special (expression_special);
	} else if (expression_parameter) {
		compile_expression_parameter (expression_parameter);
	} else if (expression_compound) {
		compile_expression_compound (expression_compound);
	}
}


void Program::compile_expression_string (shared_ptr <ExpressionString> a_expression) {
	instructions.push_back (make_shared <LoadValue> (
		make_shared <StringValue> (a_expression->value)
	));
}


void Program::compile_expression_special (shared_ptr <ExpressionSpecial> a_expression) {
	if (a_expression->name == string {"latest"}) {
		instructions.push_back (make_shared <GetAndThenStackTop> ());
	} else if (a_expression->name == string {"true"}) {
		instructions.push_back (make_shared <LoadValue> (
			make_shared <BooleanValue> (true)
		));
	} else if (a_expression->name == string {"false"}) {
		instructions.push_back (make_shared <LoadValue> (
			make_shared <BooleanValue> (false)
		));
	} else {
		cerr << "Unknown special expression: " << a_expression->name << endl;
	}
}


void Program::compile_expression_parameter (shared_ptr <ExpressionParameter> a_expression) {
	instructions.push_back (make_shared <GetParameterValue> (
		a_expression->depth,
		a_expression->name
	));
}


void Program::compile_expression_compound (shared_ptr <ExpressionCompound> a_expression) {
	auto parameters = a_expression->parameters;

	string action;
	if (parameters.find (string {"action"}) != parameters.end ()
		&& dynamic_pointer_cast <ExpressionString> (parameters.at (string {"action"}))
	) {
		action = dynamic_pointer_cast <ExpressionString> (parameters.at (string {"action"}))->value;
	}

	if (action == string {"and"}) {
		compile_expression (parameters.at (string {"left"}));

		unsigned int jump_if_false = instructions.size ();
		instructions.push_back (make_shared <JumpIfFalse> ());

		compile_expression (parameters.at (string {"right"}));
		
		dynamic_pointer_cast <JumpIfFalse> (instructions.at (jump_if_false))->go_to = instructions.size ();
	} else if (action == string {"or"}) {
		compile_expression (parameters.at (string {"left"}));

		unsigned int jump_if_true = instructions.size ();
		instructions.push_back (make_shared <JumpIfTrue> ());

		compile_expression (parameters.at (string {"right"}));
		
		dynamic_pointer_cast <JumpIfTrue> (instructions.at (jump_if_true))->go_to = instructions.size ();
	} else if (action == string {"if"}) {
		compile_expression (parameters.at (string {"if"}));

		unsigned int jump_if_false = instructions.size ();
		instructions.push_back (make_shared <JumpIfFalse> ());

		compile_expression (parameters.at (string {"then"}));

		unsigned int jump = instructions.size ();
		instructions.push_back (make_shared <Jump> ());

		dynamic_pointer_cast <JumpIfFalse> (instructions.at (jump_if_false))->go_to = instructions.size ();

		compile_expression (parameters.at (string {"else"}));
		
		dynamic_pointer_cast <Jump> (instructions.at (jump))->go_to = instructions.size ();
	} else if (action == string {"andthen"}) {
		compile_expression (parameters.at (string {"left"}));

		instructions.push_back (make_shared <PushAndThenStack> ());

		compile_expression (parameters.at (string {"right"}));

		instructions.push_back (make_shared <PopAndThenStack> ());
	} else {
		instructions.push_back (make_shared <CreateParameterStore> ());

		for (auto parameter: parameters) {
			string parameter_name = parameter.first;
			shared_ptr <Expression> parameter_value = parameter.second;
			
			compile_expression (parameter_value);
			
			instructions.push_back (make_shared <StoreParameter> (parameter_name));
		}
		
		put_argument_resolution_instructions ();
	}
}


