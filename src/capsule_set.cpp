#include "todes.h"


using namespace std;
using namespace todes;


void CapsuleSet::run ()
{
	auto method_value = get_parameter (string {"method"});
	auto method_string = dynamic_pointer_cast <StringValue> (method_value);
	if (! method_string) {
		return;
	}
	string method = method_string->value;

	auto main_value = get_parameter ();
	
	if (method == string {"isset"}) {
		set_return_value (make_shared <BooleanValue> (true));
	} else if (method == string {"size"}) {
		set_return_value (make_shared <NumberValue> (container->size ()));
	} else if (method == string {"includes"}) {
		set_return_value (make_shared <BooleanValue> (
			container->find (main_value) != container->end ()
		));
	} else if (method == string {"erase"}) {
		auto new_container = make_shared <set <shared_ptr <Value>, Comparer>> (comparer);
		new_container->insert (container->begin (), container->end ());
		if (new_container->find (main_value) != new_container->end ()) {
			new_container->erase (main_value);
		}
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleSet> (new_container, comparer)
		));
	} else if (method == string ("first")) {
		auto first = container->begin ();
		if (first != container->end ()) {
			set_return_value (* first);
		}
	} else if (method == string ("next")) {
		auto found = container->find (main_value);
		if (found != container->end ()) {
			found ++;
			if (found != container->end ()) {
				set_return_value (* found);
			}
		}
	}
}


shared_ptr <CapsuleAppendable>
	CapsuleSet::append (shared_ptr <class Value> a_value)
{
	auto new_container = make_shared <set <shared_ptr <Value>, Comparer>> (comparer);
	new_container->insert (container->begin (), container->end ());
	if (new_container->find (a_value) == new_container->end ()) {
		new_container->insert (a_value);
	}
	return make_shared <CapsuleSet> (new_container, comparer);
}


