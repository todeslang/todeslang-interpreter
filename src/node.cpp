#include "picojson.h"

#include "todes.h"


using namespace std;
using namespace todes;


void NodeProgram::read (ifstream &a_in)
{
	for (; ; ) {
		if (a_in.eof ()) {
			break;
		}

		string buffer;
		getline (a_in, buffer);
		
		if (! buffer.empty ()) {
			NodeDirective node_directive;
			node_directive.read (buffer);
			directives.push_back (node_directive);
		}
	}
}


void NodeDirective::read (string a_in)
{
	picojson::value directive_value;
	string error = picojson::parse (directive_value, a_in);
	if (! error.empty ()) {
		cerr << error << endl;
		throw (InvalidProgramException {});
	}

	try {
		auto directive_object = directive_value.get <picojson::object> ();

		name = directive_object.at (string {"name"}).get <string> ();

		for (auto parameter_value:
			directive_object.at (string {"parameters"}).get <picojson::array> ()
		) {
			auto parameter_object = parameter_value.get <picojson::object> ();
			string parameter_name = parameter_object.at (string {"name"}).get <string> ();
			auto parameter_value_value = parameter_object.at (string {"value"});
			auto parameter_value_expression = read_expression (parameter_value_value);
			parameters.insert (pair <string, shared_ptr <Expression>> {
				parameter_name,
				parameter_value_expression
			});
		}
		
	} catch (exception &e) {
		throw (InvalidProgramException {});
	}
	
}


void ExpressionCompound::read (picojson::object a_in)
{
	for (auto parameter_value:
		a_in.at (string {"parameters"}).get <picojson::array> ()
	) {
		auto parameter_object = parameter_value.get <picojson::object> ();
		string parameter_name = parameter_object.at (string {"name"}).get <string> ();
		auto parameter_value_value = parameter_object.at (string {"value"});
		auto parameter_value_expression = read_expression (parameter_value_value);
		parameters.insert (pair <string, shared_ptr <Expression>> {
			parameter_name,
			parameter_value_expression
		});
	}
}


shared_ptr <Expression> todes::read_expression (picojson::value a_value)
{
	auto input_object = a_value.get <picojson::object> ();
	string class_name = input_object.at (string {"class"}).get <string> ();
	shared_ptr <Expression> expression;

	if (class_name == string {"expressionString"}) {
		auto expression_string = make_shared <ExpressionString> ();
		expression_string->read (input_object);
		expression = expression_string;
	} else if (class_name == string {"expressionSpecial"}) {
		auto expression_special = make_shared <ExpressionSpecial> ();
		expression_special->read (input_object);
		expression = expression_special;
	} else if (class_name == string {"expressionParameter"}) {
		auto expression_parameter = make_shared <ExpressionParameter> ();
		expression_parameter->read (input_object);
		expression = expression_parameter;
	} else if (class_name == string {"expressionCompound"}) {
		auto expression_compound = make_shared <ExpressionCompound> ();
		expression_compound->read (input_object);
		expression = expression_compound;
	} else {
		cerr << "Unknown expression: " << class_name << endl;
		throw (InvalidProgramException {});
	}

	return expression;
}


