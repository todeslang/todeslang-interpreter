#include <sstream>
#include <limits>

#include "todes.h"


using namespace std;
using namespace todes;


void CreateParameterStore::run (class DynamicEnvironment &a_env)
{
	a_env.parameter_store_stack.push_back (map <string, shared_ptr <Value>> {});
}


void LoadValue::run (class DynamicEnvironment &a_env)
{
	a_env.working_register = value;
}


string LoadValue::format () const
{
	return string {"LoadValue: "} + value->format ();
}


void StoreParameter::run (class DynamicEnvironment &a_env)
{
	a_env.parameter_store_stack.back ().insert (
		pair <string, shared_ptr <Value>> {
			parameter_name,
			a_env.working_register,
		}
	);
}


void Exit::run (class DynamicEnvironment &a_env)
{
	a_env.program_counter = numeric_limits <unsigned int>::max ();
}


void CreateArgumentResolutionEnvironment::run (class DynamicEnvironment &a_env)
{
	ArgumentResolutionEnvironment argument_resolution_environment;

	argument_resolution_environment.start_point_of_argument_resolution_step
		= start_point_of_argument_resolution_step;

	argument_resolution_environment.end_point_of_argument_resolution
		= end_point_of_argument_resolution;

	argument_resolution_environment.anchor = numeric_limits <unsigned int>::max ();

	auto parameters = a_env.parameter_store_stack.back ();
	a_env.parameter_store_stack.pop_back ();

	argument_resolution_environment.current_parameters = parameters;
	argument_resolution_environment.next_parameters = parameters;

	a_env.argument_resolution_environment_stack.push_back
		(argument_resolution_environment);
}


string CreateArgumentResolutionEnvironment::format () const
{
	stringstream stream;
	stream
		<< start_point_of_argument_resolution_step << " "
		<< end_point_of_argument_resolution;
	return string {"CreateArgumentResolutionEnvironment: "}
		+ stream.str ();
}


void TryDirectives::run (class DynamicEnvironment &a_env)
{
	auto & argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	if (! a_env.static_environment->program.directives.empty ()) {
		argument_resolution_environment.current_directive_index
			= a_env.static_environment->program.directives.size () - 1;
		argument_resolution_environment.go_if_all_directives_inactive = a_env.program_counter;
		a_env.program_counter
			= a_env.static_environment->program.directives
				.at (argument_resolution_environment.current_directive_index);
	}
}


void TryCapsule::run (class DynamicEnvironment &a_env)
{
	auto argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	auto parameters = argument_resolution_environment.current_parameters;

	if (parameters.find (string {"action"}) == parameters.end ()) {
		return;
	}

	auto action_value = parameters.at (string {"action"});
	auto action_capsule = dynamic_pointer_cast <CapsuleValue> (action_value);

	if (! action_capsule) {
		return;
	}
	
	auto capsule = action_capsule->value;
	
	capsule->initialize (& a_env);
	capsule->run ();

	a_env.working_register = capsule->return_value;
	a_env.program_counter
		= argument_resolution_environment.end_point_of_argument_resolution;
}


void TryContinuation::run (class DynamicEnvironment &a_env)
{
	auto argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	auto current_parameters = argument_resolution_environment.current_parameters;

	if (current_parameters.find (string {"action"}) != current_parameters.end ()
		&& dynamic_pointer_cast <ContinuationValue>
			(current_parameters.at (string {"action"}))
	) {
		auto continuation = dynamic_pointer_cast <ContinuationValue>
			(current_parameters.at (string {"action"}));
		shared_ptr <DynamicEnvironment> env_p
			= continuation->get_dynamic_environment_ptr ();
		if (env_p) {
			DynamicEnvironment env = (* env_p);
			if (current_parameters.find (string {"main"}) == current_parameters.end ()) {
				env.continuation_parameter = make_shared <BooleanValue> (false);
			} else {
				env.continuation_parameter = current_parameters.at (string {"main"});
			}
			a_env = env;
		} else {
			cerr << "WARNING: Continuation already released." << endl;
		}
	}
}


void TryTerminalContinuation::run (class DynamicEnvironment &a_env)
{
	auto argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	auto current_parameters = argument_resolution_environment.current_parameters;

	if (current_parameters.find (string {"action"}) != current_parameters.end ()
		&& dynamic_pointer_cast <TerminalContinuationValue>
			(current_parameters.at (string {"action"}))
	) {
		a_env.program_counter = numeric_limits<unsigned int>::max ();
		if (current_parameters.find (string {"main"}) == current_parameters.end ()) {
			a_env.working_register = make_shared <BooleanValue> (false);
		} else {
			a_env.working_register = current_parameters.at (string {"main"});
		}
	}
}


void TryPrimitive::run (class DynamicEnvironment &a_env)
{
	auto argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	auto parameters = argument_resolution_environment.current_parameters;

	if (parameters.find (string {"action"}) == parameters.end ()) {
		return;
	}

	auto action_value = parameters.at (string {"action"});
	auto action_string = dynamic_pointer_cast <StringValue> (action_value);

	if (! action_string) {
		return;
	}
	
	if (a_env.static_environment->primitives.find (action_string->value)
		== a_env.static_environment->primitives.end ()
	) {
		return;
	}

	auto primitive = a_env.static_environment->primitives.at (action_string->value);
	
	primitive->initialize (& a_env);
	primitive->run ();

	a_env.working_register = primitive->return_value;
	a_env.program_counter
		= argument_resolution_environment.end_point_of_argument_resolution;
}


void FinalFallback::run (class DynamicEnvironment &a_env)
{
	auto argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	a_env.working_register = make_shared <BooleanValue> (false);
	a_env.program_counter
		= argument_resolution_environment.end_point_of_argument_resolution;
}


void DeleteArgumentResolutionEnvironment::run (class DynamicEnvironment &a_env)
{
	a_env.argument_resolution_environment_stack.pop_back ();
}


void JumpIfLowerThanAnchor::run (class DynamicEnvironment &a_env)
{
	auto argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	if (argument_resolution_environment.anchor <= directive_start_point) {
		a_env.program_counter = go_to;
	}
}


string JumpIfLowerThanAnchor::format () const
{
	stringstream stream;
	stream
		<< directive_start_point << " "
		<< go_to;
	return string {"JumpIfLowerThanAnchor: "} + stream.str ();
};


void TryNextDirective::run (class DynamicEnvironment &a_env)
{
	auto & argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();

	if (argument_resolution_environment.current_directive_index == 0) {
		a_env.program_counter = argument_resolution_environment.go_if_all_directives_inactive;
	} else {
		argument_resolution_environment.current_directive_index --;
		a_env.program_counter
			= a_env.static_environment->program.directives.at
			(argument_resolution_environment.current_directive_index);
	}
}


void PushHierarchy::run (class DynamicEnvironment &a_env)
{
	if (! dynamic_pointer_cast <NumberValue> (a_env.working_register)) {
		cerr
			<< "Hierarchy must be a natural number but: "
			<< a_env.working_register->format ()
			<< endl;
		throw (InvalidProgramException {});
	}
	
	a_env.hierarchy_stack.push_back
		(dynamic_pointer_cast <NumberValue> (a_env.working_register)->value);
}


void PopHierarchy::run (class DynamicEnvironment &a_env)
{
	a_env.hierarchy_stack.pop_back ();
}


void JumpIfBiggerThanHierarchy::run (class DynamicEnvironment &a_env)
{
	if (! dynamic_pointer_cast <NumberValue> (a_env.working_register)) {
		cerr
			<< "Hierarchy must be a natural number but: "
			<< a_env.working_register->format ()
			<< endl;
		throw (InvalidProgramException {});
	}
	
	unsigned int hierarchy_of_this_directive
		= dynamic_pointer_cast <NumberValue> (a_env.working_register)->value;
	
	if ((! a_env.hierarchy_stack.empty ())
		&& a_env.hierarchy_stack.back () <= hierarchy_of_this_directive
	) {
		a_env.program_counter = go_to;
	}
}


string JumpIfBiggerThanHierarchy::format () const
{
	stringstream stream;
	stream << go_to;
	return string {"JumpIfBiggerThanHierarchy: "} + stream.str ();
};


void Jump::run (class DynamicEnvironment &a_env)
{
	a_env.program_counter = go_to;
}


string Jump::format () const
{
	stringstream stream;
	stream << go_to;
	return string {"Jump: "} + stream.str ();
};


void JumpIfTrue::run (class DynamicEnvironment &a_env)
{
	if (a_env.working_register->is_true ()) {
		a_env.program_counter = go_to;
	}
}


string JumpIfTrue::format () const
{
	stringstream stream;
	stream << go_to;
	return string {"JumpIfTrue: "} + stream.str ();
};


void JumpIfFalse::run (class DynamicEnvironment &a_env)
{
	if (! a_env.working_register->is_true ()) {
		a_env.program_counter = go_to;
	}
}


string JumpIfFalse::format () const
{
	stringstream stream;
	stream << go_to;
	return string {"JumpIfFalse: "} + stream.str ();
};


void UpdateArgumentResolutionParameter::run (class DynamicEnvironment &a_env)
{
	auto & argument_resolution_environment = a_env.argument_resolution_environment_stack.back ();
	auto & next_parameters = argument_resolution_environment.next_parameters;
	auto value = a_env.working_register;
	if (next_parameters.find (name) == next_parameters.end ()) {
		next_parameters.insert (pair <string, shared_ptr <Value>> {
			name,
			value
		});
	} else {
		next_parameters.at (name) = value;
	}
}


string UpdateArgumentResolutionParameter::format () const
{
	return string {"UpdateArgumentResolutionParameter: "} + name;
}


void UpdateParameterGeneration::run (class DynamicEnvironment &a_env)
{
	auto & argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	argument_resolution_environment.current_parameters
		= argument_resolution_environment.next_parameters;
}


void RestartArgumentResolution::run (class DynamicEnvironment &a_env)
{
	auto argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	a_env.program_counter
		= argument_resolution_environment.start_point_of_argument_resolution_step;
}


void GetAndThenStackTop::run (class DynamicEnvironment &a_env)
{
	if (a_env.and_then_stack.empty ()) {
		 a_env.working_register = make_shared <BooleanValue> (false);
	} else {
		 a_env.working_register = a_env.and_then_stack.back ();
	}
}


void GetParameterValue::run (class DynamicEnvironment &a_env)
{
	if (depth == 0) {
		if (a_env.argument_resolution_environment_stack.empty ()) {
			a_env.working_register = make_shared <BooleanValue> (false);
		} else {
			auto environment = a_env.argument_resolution_environment_stack.back ();
			auto parameters = environment.current_parameters;
			if (parameters.find (name) == parameters.end ()) {
				a_env.working_register = make_shared <BooleanValue> (false);
			} else {
				a_env.working_register = parameters.at (name);
			}
		}
	} else if (depth == 1) {
		a_env.working_register = make_shared <BooleanValue> (false);

		for (auto environment = a_env.argument_resolution_environment_stack.rbegin ();
			environment != a_env.argument_resolution_environment_stack.rend ();
			environment ++
		) {
			auto parameters = environment->current_parameters;
			if (parameters.find (name) != parameters.end ()
				&& parameters.at (name)->is_true ()
			) {
				a_env.working_register = parameters.at (name);
				break;
			}
		}
	} else if (depth == 2) {
		vector <shared_ptr <Value>> context;

		for (auto environment = a_env.argument_resolution_environment_stack.rbegin ();
			environment != a_env.argument_resolution_environment_stack.rend ();
			environment ++
		) {
			auto parameters = environment->current_parameters;
			if (parameters.find (name) != parameters.end ()
				&& parameters.at (name)->is_true ()
			) {
				context.push_back (parameters.at (name));
			} else {
				context.push_back (make_shared <BooleanValue> (false));
			}
		}

		a_env.working_register
			= make_shared <CapsuleValue> (make_shared <CapsuleArray> (context));
	} else {
		a_env.working_register = make_shared <BooleanValue> (false);
	}
}


string GetParameterValue::format () const
{
	stringstream stream;
	stream << depth;
	return
		string {"GetParameterValue: "}
		+ stream.str () + string {" "}
		+ name;
}


void PushAndThenStack::run (class DynamicEnvironment &a_env)
{
	a_env.and_then_stack.push_back (a_env.working_register);
}


void PopAndThenStack::run (class DynamicEnvironment &a_env)
{
	a_env.and_then_stack.pop_back ();
}


void CreateContinuation::run (class DynamicEnvironment &a_env)
{
	auto env = make_shared <DynamicEnvironment> (a_env);
	env->program_counter = go_to;
	unsigned int index = a_env.static_environment->issuer ();
	a_env.static_environment->continuations.insert (
		pair <unsigned int, shared_ptr <DynamicEnvironment>> {
			index, env
		}
	);
	auto continuation = make_shared <ContinuationValue> (
		a_env.static_environment,
		index
	);
	auto & argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	argument_resolution_environment.continuation = continuation;
}


string CreateContinuation::format () const
{
	stringstream stream;
	stream << go_to;
	return string {"CreateContinuation: "} + stream.str ();
}


void RestoreParameterGeneration::run (class DynamicEnvironment &a_env)
{
	auto & argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	argument_resolution_environment.next_parameters
		= argument_resolution_environment.current_parameters;
}


void SetAnchor::run (class DynamicEnvironment &a_env)
{
	auto & argument_resolution_environment
		= a_env.argument_resolution_environment_stack.back ();
	argument_resolution_environment.anchor = anchor;
}


string SetAnchor::format () const
{
	stringstream stream;
	stream << anchor;
	return string {"SetAnchor: "} + stream.str ();
}

