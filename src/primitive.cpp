#include "todes.h"


using namespace std;
using namespace todes;


void Primitive::initialize (class DynamicEnvironment *a_dynamic_environment)
{
	dynamic_environment = a_dynamic_environment;
	return_value = make_shared <BooleanValue> (false);
}


shared_ptr <class Value> Primitive::get_parameter ()
{
	return get_parameter (string {"main"});
}


shared_ptr <class Value> Primitive::get_parameter (string a_name)
{
	auto parameters = dynamic_environment
		->argument_resolution_environment_stack.back ()
		.current_parameters;

	shared_ptr <Value> value;

	if (parameters.find (a_name) == parameters.end ()) {
		value = make_shared <BooleanValue> (false);
	} else {
		value = parameters.at (a_name);
	}
	
	return value;
}


void Primitive::set_return_value (shared_ptr <class Value> a_value)
{
	return_value = a_value;
}


void Primitive::throw_guest_catchable_exception ()
{
	throw_guest_catchable_exception (make_shared <BooleanValue> (false));
}


void Primitive::throw_guest_catchable_exception (shared_ptr <class Value> a_value)
{
	if (dynamic_environment->exception_stack.empty ()) {
		throw (RuntimeException {a_value});
	}
	shared_ptr <DynamicEnvironment> env_p
		= dynamic_environment->exception_stack.back ();
	DynamicEnvironment env = (* env_p);
	env.continuation_parameter = a_value;
	(* dynamic_environment) = env;
}


