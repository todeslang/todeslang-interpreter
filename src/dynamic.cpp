#include <limits>
#include <sstream>

#include "todes.h"


using namespace std;
using namespace todes;


void DynamicEnvironment::bootstrap ()
{
	program_counter = 0;
	continuation_parameter = make_shared <BooleanValue> (false);
}


string DynamicEnvironment::dump () const
{
	stringstream stream;
	stream
		<< "{" << endl
		<< "\t" << "\"argumentResolutionEnvironmentDepth\":"
			<< argument_resolution_environment_stack.size () << "," << endl;
	if (! argument_resolution_environment_stack.empty ()) {
		stream
			<< "\t\"parameters\":[" << endl;
		auto parameters = argument_resolution_environment_stack.back ().current_parameters;
		for (auto parameter: parameters) {
			stream
				<< "\t\t{\"" << parameter.first << "\":\""
				<< parameter.second->format () << "\"}," << endl;
		}
		stream
			<< "\t]" << endl;
	}
	stream
		<< "}" << endl;
	return stream.str ();
}


shared_ptr <class Value> DynamicEnvironment::run
        (shared_ptr <class Value> a_continuation_parameter)
{
	return static_environment->run (* this, a_continuation_parameter);
}


