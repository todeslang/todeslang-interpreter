#include "todes.h"


using namespace std;
using namespace todes;


bool Comparer::operator ()
	(const shared_ptr <class Value> & left, const shared_ptr <class Value> & right) const
{
	bool return_value = false;

	if (continuation) {
		auto continuation_parameter = make_shared <CapsuleValue> (
			make_shared <CapsuleArray> (vector <shared_ptr <Value>> {
				left,
				right
			})
		);
		auto terminal_continuation_parameter = continuation->run (continuation_parameter);
		return_value = dynamic_pointer_cast <BooleanValue> (terminal_continuation_parameter)->value;
	} else {
		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		auto string_left = dynamic_pointer_cast <StringValue> (left);
		auto string_right = dynamic_pointer_cast <StringValue> (right);
		return_value
		=	(number_left && number_right && number_left->value < number_right->value)
		||	(string_left && string_right && string_left->value < string_right->value);
	}
	
	return return_value;
}

