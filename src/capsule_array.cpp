#include "todes.h"


using namespace std;
using namespace todes;


void CapsuleArray::run ()
{
	auto method_value = get_parameter (string {"method"});
	auto method_string = dynamic_pointer_cast <StringValue> (method_value);
	if (! method_string) {
		return;
	}
	string method = method_string->value;

	auto main_value = get_parameter ();
	auto main_number = dynamic_pointer_cast <NumberValue> (main_value);
	auto main_string = dynamic_pointer_cast <StringValue> (main_value);
	auto at_number = dynamic_pointer_cast <NumberValue> (get_parameter (string {"at"}));
	
	if (method == string {"isarray"}) {
		set_return_value (make_shared <BooleanValue> (true));
	} else if (method == string {"size"}) {
		set_return_value (make_shared <NumberValue> (container.size ()));
	} else if (method == string {"at"}) {
		if (main_number) {
			set_return_value (container.at (main_number->value));
		}
	} else if (method == string {"replace"}) {
		if (at_number) {
			auto new_container = container;
			new_container.at (at_number->value) = get_parameter ("by");
			set_return_value (make_shared <CapsuleValue> (
				make_shared <CapsuleArray> (new_container)
			));
		}
	} else if (method == string {"tostring"}) {
		string s;
		for (auto value: container) {
			auto number_value = dynamic_pointer_cast <NumberValue> (value);
			if (! number_value) {
				throw_guest_catchable_exception ();
			}
			s.push_back (static_cast <char> (number_value->value));
		}
		set_return_value (make_shared <StringValue> (s));
	} else if (method == string {"fromstring"}) {
		auto string_main = dynamic_pointer_cast <StringValue> (get_parameter ());
		if (! string_main) {
			throw_guest_catchable_exception ();
		}
		vector <shared_ptr <Value>> new_container;
		for (char c: string_main->value) {
			unsigned int number = static_cast <unsigned int> (static_cast <unsigned char> (c));
			new_container.push_back (make_shared <NumberValue> (number));
			set_return_value (make_shared <CapsuleValue> (
				make_shared <CapsuleArray> (new_container)
			));
		}
	}
}


shared_ptr <CapsuleAppendable> CapsuleArray::append
	(shared_ptr <class Value> a_value)
{
	auto new_container = container;
	new_container.push_back (a_value);
	return make_shared <CapsuleArray> (new_container);
}


