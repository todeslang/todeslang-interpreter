#include "todes.h"


using namespace std;
using namespace todes;


void CapsuleFile::run ()
{
	auto method_value = get_parameter (string {"method"});
	auto method_string = dynamic_pointer_cast <StringValue> (method_value);
	if (! method_string) {
		return;
	}
	string method = method_string->value;

	auto main_value = get_parameter ();
	auto main_string = dynamic_pointer_cast <StringValue> (main_value);
	auto main_capsule = dynamic_pointer_cast <CapsuleValue> (main_value);
	
	if (method == string {"isfile"}) {
		set_return_value (make_shared <BooleanValue> (true));
	} else if (method == string {"readopen"}) {
		file = fopen (main_string->value.c_str (), "rb");
	} else if (method == string {"writeopen"}) {
		file = fopen (main_string->value.c_str (), "wb");
	} else if (method == string {"appendopen"}) {
		file = fopen (main_string->value.c_str (), "ab");
	} else if (method == string {"close"}) {
		fclose (file);
	} else if (method == string {"stdin"}) {
		file = stdin;
	} else if (method == string {"stdout"}) {
		file = stdout;
	} else if (method == string {"stderr"}) {
		file = stderr;
	} else if (method == string {"isopen"}) {
		set_return_value (make_shared <BooleanValue> (file != nullptr));
	} else if (method == string {"readstring"}) {
		string buffer_1;
		char * buffer_2 = static_cast <char *> (malloc (1024));
		for (; ; ) {
			auto fgets_return = fgets (buffer_2, 1024, file);
			if (fgets_return == nullptr) {
				break;
			}
			buffer_1 += string {buffer_2};
		}
		set_return_value (make_shared <StringValue> (buffer_2));
	} else if (method == string {"readbinary"}) {
		vector <unsigned int> buffer;
		int c = 0;
		for (; ; ) {
			c = getc (file);
			if (c == EOF) {
				break;
			}
			buffer.push_back (c);
		}
		auto capsule_array = make_shared <CapsuleArray> ();
		for (auto b: buffer) {
			capsule_array->container.push_back (make_shared <NumberValue> (b));
		}
		set_return_value (make_shared <CapsuleValue> (capsule_array));
	} else if (method == string {"writestring"}) {
		fprintf (file, "%s", main_string->value.c_str ());
	} else if (method == string {"writebinary"}) {
		auto main_capsule_array = dynamic_pointer_cast <CapsuleArray> (main_capsule);
		for (auto value: main_capsule_array->container) {
			auto number_value = dynamic_pointer_cast <NumberValue> (value);
			if (number_value) {
				fputc (number_value->value, file);
			}
		}
	}
}


