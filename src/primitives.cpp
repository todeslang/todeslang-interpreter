#include <sstream>

#include "todes.h"


using namespace std;
using namespace todes;


/* Operators */


static bool equal (shared_ptr <Value> left, shared_ptr <Value> right)
{
	auto number_left = dynamic_pointer_cast <NumberValue> (left);
	auto number_right = dynamic_pointer_cast <NumberValue> (right);
	auto string_left = dynamic_pointer_cast <StringValue> (left);
	auto string_right = dynamic_pointer_cast <StringValue> (right);
	auto boolean_left = dynamic_pointer_cast <BooleanValue> (left);
	auto boolean_right = dynamic_pointer_cast <BooleanValue> (right);
	/* ContinuationValue */
	auto terminal_continuation_left = dynamic_pointer_cast <TerminalContinuationValue> (left);
	auto terminal_continuation_right = dynamic_pointer_cast <TerminalContinuationValue> (right);
	/* CapsuleValue */
	auto pair_left = dynamic_pointer_cast <PairValue> (left);
	auto pair_right = dynamic_pointer_cast <PairValue> (right);
	auto broken_pair_left = dynamic_pointer_cast <BrokenPairValue> (left);
	auto broken_pair_right = dynamic_pointer_cast <BrokenPairValue> (right);

	return
		(number_left && number_right && number_left->value == number_right->value)
		|| (string_left && string_right && string_left->value == string_right->value)
		|| (boolean_left && boolean_right && boolean_left->value == boolean_right->value)
		|| (terminal_continuation_left && terminal_continuation_right)
		|| (pair_left
			&& pair_right
			&& equal (pair_left->key, pair_right->key)
			&& equal (pair_left->value, pair_right->value)
		   )
		|| (broken_pair_left
			&& broken_pair_right
			&& equal (broken_pair_left->key, broken_pair_right->key)
		   );
}


class PrimitiveEqual: public Primitive {
public:
	void run () {
		set_return_value (make_shared <BooleanValue> (
			equal (get_parameter (string {"left"}), get_parameter (string {"right"}))
		));
	};
};


class PrimitiveLess: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		auto string_left = dynamic_pointer_cast <StringValue> (left);
		auto string_right = dynamic_pointer_cast <StringValue> (right);
		auto boolean_left = dynamic_pointer_cast <BooleanValue> (left);
		auto boolean_right = dynamic_pointer_cast <BooleanValue> (right);
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */

		bool return_value
		=	(number_left && number_right && number_left->value < number_right->value)
		||	(string_left && string_right && string_left->value < string_right->value)
		||	(boolean_left && boolean_right && (boolean_left->value? 1: 0) < (boolean_right->value? 1: 0));
		
		set_return_value (make_shared <BooleanValue> (return_value));
	};
};


class PrimitiveLessOrEqual: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		auto string_left = dynamic_pointer_cast <StringValue> (left);
		auto string_right = dynamic_pointer_cast <StringValue> (right);
		auto boolean_left = dynamic_pointer_cast <BooleanValue> (left);
		auto boolean_right = dynamic_pointer_cast <BooleanValue> (right);
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */

		bool return_value
		=	(number_left && number_right && number_left->value <= number_right->value)
		||	(string_left && string_right && string_left->value <= string_right->value)
		||	(boolean_left && boolean_right && (boolean_left->value? 1: 0) <= (boolean_right->value? 1: 0));
		
		set_return_value (make_shared <BooleanValue> (return_value));
	};
};


class PrimitiveIs: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto string_right = dynamic_pointer_cast <StringValue> (right);
		if (! string_right) {
			return;
		}
		
		string name = string_right->value;

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto string_left = dynamic_pointer_cast <StringValue> (left);
		auto boolean_left = dynamic_pointer_cast <BooleanValue> (left);
		auto continuation_left = dynamic_pointer_cast <ContinuationValue> (left);
		auto terminal_continuation_left = dynamic_pointer_cast <ContinuationValue> (left);
		auto capsule_left = dynamic_pointer_cast <TerminalContinuationValue> (left);
		auto pair_left = dynamic_pointer_cast <PairValue> (left);
		auto broken_pair_left = dynamic_pointer_cast <BrokenPairValue> (left);

		bool return_value
		=	(number_left && name == string {"number"})
		||	(string_left && name == string {"string"})
		||	(boolean_left && name == string {"boolean"})
		||	(continuation_left && name == string {"continuation"})
		||	(terminal_continuation_left && name == string {"terminalcontinuation"})
		||	(capsule_left && name == string {"capsule"})
		||	(pair_left && name == string {"pair"})
		||	(broken_pair_left && name == string {"brokenpair"});
		
		set_return_value (make_shared <BooleanValue> (return_value));
	};
};


class PrimitivePair: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});
		set_return_value (make_shared <PairValue> (left, right));
	};
};


class PrimitivePlus: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		/* StringValue */
		/* BooleanValue */
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */

		if (number_left && number_right) {
			set_return_value (make_shared <NumberValue> (
				number_left->value + number_right->value
			));
		}
	};
};


class PrimitiveMinus: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		/* StringValue */
		/* BooleanValue */
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */

		if (number_left && number_right) {
			set_return_value (make_shared <NumberValue> (
				number_left->value - number_right->value
			));
		}
	};
};


class PrimitiveMultiply: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		/* StringValue */
		/* BooleanValue */
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */

		if (number_left && number_right) {
			set_return_value (make_shared <NumberValue> (
				number_left->value * number_right->value
			));
		}
	};
};


class PrimitiveDivide: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		/* StringValue */
		/* BooleanValue */
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */

		if (number_left && number_right) {
			set_return_value (make_shared <NumberValue> (
				number_left->value / number_right->value
			));
		}
	};
};


class PrimitiveAppend: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		/* NumberValue */
		auto string_left = dynamic_pointer_cast <StringValue> (left);
		auto string_right = dynamic_pointer_cast <StringValue> (right);
		/* BooleanValue */
		/* ContinuationValue */
		/* TerminalContinuationValue */
		auto capsule_left = dynamic_pointer_cast <CapsuleValue> (left);

		if (string_left) {
			if (string_right) {
				set_return_value
					(make_shared <StringValue> (string_left->value + string_right->value));
			} else {
				set_return_value
					(make_shared <StringValue> (string_left->value + right->format ()));
			}
		} else if (capsule_left) {
			auto capsule_appendable = dynamic_pointer_cast
				<CapsuleAppendable> (capsule_left->value);
			set_return_value (
				make_shared <CapsuleValue> (capsule_appendable->append (right))
			);
		}
	};
};


class PrimitiveAs: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto string_right = dynamic_pointer_cast <StringValue> (right);
		if (! string_right) {
			return;
		}
		
		string name = string_right->value;

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto string_left = dynamic_pointer_cast <StringValue> (left);
		auto boolean_left = dynamic_pointer_cast <BooleanValue> (left);
		auto continuation_left = dynamic_pointer_cast <ContinuationValue> (left);
		auto terminal_continuation_left = dynamic_pointer_cast <ContinuationValue> (left);
		auto capsule_left = dynamic_pointer_cast <TerminalContinuationValue> (left);
		auto pair_left = dynamic_pointer_cast <PairValue> (left);
		auto broken_pair_left = dynamic_pointer_cast <BrokenPairValue> (left);

		if (name == string {"number"}) {
			if (number_left) {
				set_return_value (number_left);
			}
		} else if (name == string {"string"}) {
			if (string_left) {
				set_return_value (string_left);
			} else {
				set_return_value (make_shared <StringValue> (left->format ()));
			}
		} else if (name == string {"boolean"}) {
			set_return_value (make_shared <BooleanValue> (left->is_true ()));
		} else if (name == string {"continuation"}) {
			if (continuation_left) {
				set_return_value (continuation_left);
			}
		} else if (name == string {"terminalcontinuation"}) {
			if (terminal_continuation_left) {
				set_return_value (terminal_continuation_left);
			}
		} else if (name == string {"capsule"}) {
			if (capsule_left) {
				set_return_value (capsule_left);
			}
		} else if (name == string {"pair"}) {
			if (pair_left) {
				set_return_value (pair_left);
			}
		} else if (name == string {"brokenpair"}) {
			if (broken_pair_left) {
				set_return_value (broken_pair_left);
			}
		} else {
			/* Do nothing. */
		}
	};
};


class PrimitiveNot: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ();
		set_return_value (make_shared <BooleanValue> (! main_value->is_true ()));
	};
};


class PrimitiveStringToNumber: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ();
		auto main_string = dynamic_pointer_cast <StringValue> (main_value);
		if (! main_string) {
			return;
		}
		unsigned int number = 0;
		istringstream stream {main_string->value};
		stream >> hex >> number;
		set_return_value (make_shared <NumberValue> (number));
	};
};


class PrimitiveBrokenPair: public Primitive {
	void run () {
		auto main_value = get_parameter ();
		set_return_value (make_shared <BrokenPairValue> (main_value));
	};
};


/* Core */


class PrimitiveReleaseContinuation: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ();
		auto main_continuation = dynamic_pointer_cast <ContinuationValue> (main_value);
		if (main_continuation) {
			unsigned int index = main_continuation->index;
			map <unsigned int, shared_ptr <DynamicEnvironment>> & continuations
				= main_continuation->static_environment->continuations;
			if (continuations.find (index) != continuations.end ()) {
				continuations.erase (index);
			}
		}
	};
};


class PrimitiveGetTerminalContinuation: public Primitive {
public:
	void run () {
		set_return_value (make_shared <TerminalContinuationValue> ());
	};
};


class PrimitiveArray: public Primitive {
public:
	void run () {
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleArray> ()
		));
	};
};


class PrimitiveSet: public Primitive {
public:
	void run () {
		Comparer comparer;
		auto continuation_main = dynamic_pointer_cast <ContinuationValue> (get_parameter ());
		if (continuation_main) {
			shared_ptr <DynamicEnvironment> env_p
				= continuation_main->get_dynamic_environment_ptr ();
			if (env_p) {
				comparer.continuation = env_p;
			}
		}
		auto container = make_shared <set <shared_ptr <Value>, Comparer>> (
			set <shared_ptr <Value>, Comparer> (comparer)
		);
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleSet> (container, comparer)
		));
	};
};


class PrimitiveMap: public Primitive {
public:
	void run () {
		Comparer comparer;
		auto continuation_main = dynamic_pointer_cast <ContinuationValue> (get_parameter ());
		if (continuation_main) {
			shared_ptr <DynamicEnvironment> env_p
				= continuation_main->get_dynamic_environment_ptr ();
			if (env_p) {
				comparer.continuation = env_p;
			}
		}
		auto container = make_shared <map <
			shared_ptr <Value>,
			shared_ptr <Value>,
			Comparer
		>> (
			comparer
		);
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleMap> (container, comparer)
		));
	};
};


class PrimitiveTrivialContainer: public Primitive {
public:
	void run () {
		auto capsule = make_shared <CapsuleTrivialContainer> ();
		capsule->value = get_parameter ();
		set_return_value (make_shared <CapsuleValue> (capsule));
	};
};


class PrimitiveGetContinuation: public Primitive {
public:
	void run () {
		auto argument_resolution_environment
			= dynamic_environment->argument_resolution_environment_stack.back ();
		auto continuation_value = argument_resolution_environment.continuation;
		set_return_value (continuation_value);
	}
};


class PrimitiveGetContinuationParameter: public Primitive {
public:
	void run () {
		set_return_value (dynamic_environment->continuation_parameter);
	}
};


class PrimitivePushToExceptionStack: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ();
		auto main_continuation = dynamic_pointer_cast <ContinuationValue> (main_value);
		if (! main_continuation) {
			cerr << "WARNING: Exception must be a continuation." << endl;
			return;
		}
		shared_ptr <DynamicEnvironment> env_p
			= main_continuation->get_dynamic_environment_ptr ();
		if (! env_p) {
			cerr << "WARNING: Continuation already released." << endl;
			return;
		}
		dynamic_environment->exception_stack.push_back (env_p);
	}
};


class PrimitivePopExceptionStack: public Primitive {
public:
	void run () {
		dynamic_environment->exception_stack.pop_back ();
	}
};


class PrimitiveThrowException: public Primitive {
public:
	void run () {
		throw_guest_catchable_exception (get_parameter ());
	}
};


class PrimitiveEcho: public Primitive {
public:
	void run () {
		set_return_value (get_parameter ());
	}
};


class PrimitivePrint: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ();
		auto main_string = dynamic_pointer_cast <StringValue> (main_value);
		if (main_string) {
			cout << main_string->value;
		} else {
			cout << main_value->format ();
		}
	}
};


/* File */


class PrimitiveFile: public Primitive {
public:
	void run () {
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleFile> ()
		));
	}
};


/* Formatter */


class PrimitiveFormat: public Primitive {
public:
	void run () {
		stringstream s;

		s.imbue (std::locale::classic ());

		auto width_value = get_parameter ("width");
		auto width_number = dynamic_pointer_cast <NumberValue> (width_value);
		if (width_number) {
			s.width (width_number->value);
		}

		auto precision_value = get_parameter ("precision");
		auto precision_number = dynamic_pointer_cast <NumberValue> (precision_value);
		if (precision_number) {
			s.precision (precision_number->value);
		}

		if (get_parameter ("boolalpha")->is_true ()) {s << boolalpha; }
		if (get_parameter ("dec")->is_true ()) {s << dec; }
		if (get_parameter ("defaultfloat")->is_true ()) {s << defaultfloat; }
		if (get_parameter ("fixed")->is_true ()) {s << fixed; }
		if (get_parameter ("hex")->is_true ()) {s << hex; }
		if (get_parameter ("hexfloat")->is_true ()) {s << hexfloat; }
		if (get_parameter ("internal")->is_true ()) {s << internal; }
		if (get_parameter ("left")->is_true ()) {s << left; }
		if (get_parameter ("noboolalpha")->is_true ()) {s << noboolalpha; }
		if (get_parameter ("noshowbase")->is_true ()) {s << noshowbase; }
		if (get_parameter ("noshowpoint")->is_true ()) {s << noshowpoint; }
		if (get_parameter ("noshowpos")->is_true ()) {s << noshowpos; }
		if (get_parameter ("noskipws")->is_true ()) {s << noskipws; }
		if (get_parameter ("nounitbuf")->is_true ()) {s << nounitbuf; }
		if (get_parameter ("nouppercase")->is_true ()) {s << nouppercase; }
		if (get_parameter ("oct")->is_true ()) {s << oct; }
		if (get_parameter ("right")->is_true ()) {s << right; }
		if (get_parameter ("scientific")->is_true ()) {s << scientific; }
		if (get_parameter ("showbase")->is_true ()) {s << showbase; }
		if (get_parameter ("showpoint")->is_true ()) {s << showpoint; }
		if (get_parameter ("showpos")->is_true ()) {s << showpos; }
		if (get_parameter ("skipws")->is_true ()) {s << skipws; }
		if (get_parameter ("unitbuf")->is_true ()) {s << unitbuf; }
		if (get_parameter ("uppercase")->is_true ()) {s << uppercase; }
		
		auto main_value = get_parameter ();
		auto main_number = dynamic_pointer_cast <NumberValue> (main_value);
		if (main_number) {
			s << main_number->value;
		}
		
		set_return_value (make_shared <StringValue> (s.str ()));
	};
};


/* Registration */


map <string, shared_ptr <Primitive>> todes::get_primitives ()
{
	return map <string, shared_ptr <Primitive>> {
		/* Operators */
		{string {"equal"}, make_shared <PrimitiveEqual> ()},
		{string {"less"}, make_shared <PrimitiveLess> ()},
		{string {"lessorequal"}, make_shared <PrimitiveLessOrEqual> ()},
		{string {"is"}, make_shared <PrimitiveIs> ()},
		{string {"pair"}, make_shared <PrimitivePair> ()},
		{string {"plus"}, make_shared <PrimitivePlus> ()},
		{string {"minus"}, make_shared <PrimitiveMinus> ()},
		{string {"multiply"}, make_shared <PrimitiveMultiply> ()},
		{string {"divide"}, make_shared <PrimitiveDivide> ()},
		{string {"append"}, make_shared <PrimitiveAppend> ()},
		{string {"as"}, make_shared <PrimitiveAs> ()},
		{string {"not"}, make_shared <PrimitiveNot> ()},
		{string {"stringtonumber"}, make_shared <PrimitiveStringToNumber> ()},
		{string {"brokebpair"}, make_shared <PrimitiveBrokenPair> ()},

		/* Core */
		{string {"releasecontinuation"}, make_shared <PrimitiveReleaseContinuation> ()},
		{string {"getterminalcontinuation"}, make_shared <PrimitiveGetTerminalContinuation> ()},
		{string {"Array"}, make_shared <PrimitiveArray> ()},
		{string {"Set"}, make_shared <PrimitiveSet> ()},
		{string {"Map"}, make_shared <PrimitiveMap> ()},
		{string {"Trivialcontainer"}, make_shared <PrimitiveTrivialContainer> ()},
		{string {"getcontinuation"}, make_shared <PrimitiveGetContinuation> ()},
		{string {"getcontinuationparameter"}, make_shared <PrimitiveGetContinuationParameter> ()},
		{string {"pushtoexceptionstack"}, make_shared <PrimitivePushToExceptionStack> ()},
		{string {"popexceptionstack"}, make_shared <PrimitivePopExceptionStack> ()},
		{string {"throwexception"}, make_shared <PrimitiveThrowException> ()},
		{string {"echo"}, make_shared <PrimitiveEcho> ()},
		{string {"print"}, make_shared <PrimitivePrint> ()},
		
		/* File */
		{string {"File"}, make_shared <PrimitiveFile> ()},

		/* Format */
		{string {"format"}, make_shared <PrimitiveFormat> ()},
	};
}


