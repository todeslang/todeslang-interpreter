#include <iostream>
#include <cstdlib>
#include <fstream>

#include "todes.h"


using namespace std;
using namespace todes;


static void open_code_and_run (string file_name)
{
	NodeProgram node_program;
	{
		ifstream in {file_name};
		node_program.read (in);
	}

	Program program;
	program.compile (node_program);
	// program.dump_instructions ();

	StaticEnvironment senv;
	senv.program = program;
	senv.initialize ();
	senv.run ();
}


int main (int argc, char *argv [])
{
	if (1 < argc) {
		if (string {argv [1]} == string {"--help"}) {
			cout << "todes input.cjson" << endl;
		} else {
			open_code_and_run (string {argv [1]});
		}
	} else {
		cout << "todes input.cjson" << endl;
	}
	return 0;
}

