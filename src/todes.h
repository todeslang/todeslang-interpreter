
#ifndef TODES_H
#define TODES_H


#include <string>
#include <map>
#include <vector>
#include <fstream>
#include <memory>
#include <set>

#include "picojson.h"


namespace todes {


using namespace std;


/* Exceptions */


class InvalidProgramException: public exception {
};


class RuntimeException: public exception {
public:
	shared_ptr <class Value> hint;
public:
	RuntimeException () { };
	RuntimeException (shared_ptr <class Value> a_hint): hint (a_hint) { };
};


/* Syntactic node classes */


class Node {
};


class NodeProgram: public Node {
public:
	vector <class NodeDirective> directives;
public:
	void read (ifstream &a_in);
};


class NodeDirective: public Node {
public:
	string name;
	map <string, shared_ptr <class Expression>> parameters;
public:
	void read (string a_in);
};


class Expression: public Node {
public:
	virtual void read (picojson::object a_in) {
		/* Do nothing. */
	};
};


class ExpressionString: public Expression {
public:
	string value;
public:
	void read (picojson::object a_in) {
		value = a_in.at (string {"value"}).get <string> ();
	};
};


class ExpressionSpecial: public Expression {
public:
	string name;
public:
	void read (picojson::object a_in) {
		name = a_in.at (string {"name"}).get <string> ();
	};
};


class ExpressionParameter: public Expression {
public:
	unsigned int depth;
	string name;
public:
	void read (picojson::object a_in) {
		depth = static_cast <unsigned int> (a_in.at (string {"depth"}).get <double> ());
		name = a_in.at (string {"name"}).get <string> ();
	};
};


class ExpressionCompound: public Expression {
public:
	map <string, shared_ptr <class Expression>> parameters;
public:
	void read (picojson::object a_in);
};


shared_ptr <Expression> read_expression (picojson::value a_value);


/* Program */


class Program {
public:
	vector <shared_ptr <class Instruction>> instructions;
	vector <unsigned int> directives;
public:
	void dump_instructions () const;
	void put_bootstrap ();
	void put_argument_resolution_instructions ();
	void compile (NodeProgram a_node_program);
	void compile (NodeDirective a_node_directive);
	void compile_trans_directive (map <string, shared_ptr <class Expression>> a_parameters);
	void compile_continuation_directive (map <string, shared_ptr <class Expression>> a_parameters);
	void compile_anchor_directive (map <string, shared_ptr <class Expression>> a_parameters);
	void compile_expression (shared_ptr <class Expression> a_expression);
	void compile_expression_string (shared_ptr <class ExpressionString> a_expression);
	void compile_expression_special (shared_ptr <class ExpressionSpecial> a_expression);
	void compile_expression_parameter (shared_ptr <class ExpressionParameter> a_expression);
	void compile_expression_compound (shared_ptr <class ExpressionCompound> a_expression);
};


/* Instruction */


class Instruction {
public:
	virtual void run (class DynamicEnvironment &a_env) {
		/* Do nothing. */
	};
	virtual string format () const {
		return string {"Instruction"};
	};
};


class CreateParameterStore: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"CreateParameterStore"};
	};
};


class LoadValue: public Instruction {
public:
	shared_ptr <class Value> value;
public:
	LoadValue () { };
	LoadValue (shared_ptr <class Value> a_value): value (a_value) { };
public:
	void run (class DynamicEnvironment &a_env);
	string format () const;
};


class StoreParameter: public Instruction {
public:
	string parameter_name;
public:
	StoreParameter () { };
	StoreParameter (string a_parameter_name): parameter_name (a_parameter_name) { };
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"StoreParameter: "} + parameter_name;
	};
};


class Exit: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"Exit"};
	};
};


class CreateArgumentResolutionEnvironment: public Instruction {
public:
	unsigned int start_point_of_argument_resolution_step;
	unsigned int end_point_of_argument_resolution;
public:
	CreateArgumentResolutionEnvironment ():
		start_point_of_argument_resolution_step (0),
		end_point_of_argument_resolution (0)
		{ };
public:
	void run (class DynamicEnvironment &a_env);
	string format () const;
};


class TryDirectives: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"TryForceDirectives"};
	};
};


class TryCapsule: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"TryCapsule"};
	};
};


class TryContinuation: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"TryContinuation"};
	};
};


class TryTerminalContinuation: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"TryTerminalContinuation"};
	};
};


class TryPrimitive: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"TryPrimitive"};
	};
};


class FinalFallback: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"FinalFallback"};
	};
};


class DeleteArgumentResolutionEnvironment: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"DeleteArgumentResolutionEnvironment"};
	};
};


class JumpIfLowerThanAnchor: public Instruction {
public:
	unsigned int directive_start_point;
	unsigned int go_to;
public:
	JumpIfLowerThanAnchor ():
		directive_start_point (0),
		go_to (0)
		{ };
public:
	void run (class DynamicEnvironment &a_env);
	string format () const;
};


class TryNextDirective: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"TryNextDirective"};
	};
};


class PushHierarchy: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"PushHierarchy"};
	};
};


class PopHierarchy: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"PopHierarchy"};
	};
};


class JumpIfBiggerThanHierarchy: public Instruction {
public:
	unsigned int go_to;
public:
	JumpIfBiggerThanHierarchy (): go_to (0) { };
public:
	void run (class DynamicEnvironment &a_env);
	string format () const;
};


class Jump: public Instruction {
public:
	unsigned int go_to;
public:
	Jump (): go_to (0) { };
public:
	void run (class DynamicEnvironment &a_env);
	string format () const;
};


class JumpIfTrue: public Instruction {
public:
	unsigned int go_to;
public:
	JumpIfTrue (): go_to (0) { };
public:
	void run (class DynamicEnvironment &a_env);
	string format () const;
};


class JumpIfFalse: public Instruction {
public:
	unsigned int go_to;
public:
	JumpIfFalse (): go_to (0) { };
public:
	void run (class DynamicEnvironment &a_env);
	string format () const;
};


class UpdateArgumentResolutionParameter: public Instruction {
public:
	string name;
public:
	UpdateArgumentResolutionParameter () { };
	UpdateArgumentResolutionParameter (string a_name): name (a_name) {};
public:
	void run (class DynamicEnvironment &a_env);
	string format () const;
};


class UpdateParameterGeneration: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"UpdateParameterGeneration"};
	};
};


class RestartArgumentResolution: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"RestartArgumentResolution"};
	};
};


class GetAndThenStackTop: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"GetAndThenStackTop"};
	}
};


class GetParameterValue: public Instruction {
public:
	unsigned int depth;
	string name;
public:
	GetParameterValue () { };
	GetParameterValue (unsigned int a_depth, string a_name): depth (a_depth), name (a_name) { };
public:
	void run (class DynamicEnvironment &a_env);
	string format () const;
};


class PushAndThenStack: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"PushAndThenStack"};
	}
};


class PopAndThenStack: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"PopAndThenStack"};
	}
};


class CreateContinuation: public Instruction {
public:
	unsigned int go_to;
public:
	void run (class DynamicEnvironment &a_env);
	string format () const;
};


class RestoreParameterGeneration: public Instruction {
public:
	void run (class DynamicEnvironment &a_env);
	string format () const {
		return string {"CreateContinuation"};
	}
};


class SetAnchor: public Instruction {
public:
	unsigned int anchor;
public:
	void run (class DynamicEnvironment &a_env);
	string format () const;
};


/* Value */


class Value {
public:
	virtual string format () const {
		return string {"VALUE"};
	};
	virtual bool is_true () const {
		return true;
	};
};


class NumberValue: public Value {
public:
	unsigned int value;
public:
	NumberValue () { };
	NumberValue (unsigned int a_value): value (a_value) { };
public:
	string format () const;
};


class StringValue: public Value {
public:
	string value;
public:
	StringValue () { };
	StringValue (string a_value): value (a_value) { };
public:
	string format () const {
		return value;
	};
};


class BooleanValue: public Value {
public:
	bool value;
public:
	BooleanValue () { };
	BooleanValue (bool a_value): value (a_value) { };
public:
	bool is_true () const {
		return value;
	};
	string format () const {
		return (value? string {"^true"}: string {"^false"});
	};
};


class ContinuationValue: public Value {
public:
	class StaticEnvironment *static_environment;
	unsigned int index;
public:
	ContinuationValue ():
		static_environment (nullptr),
		index (0)
		{ };
	ContinuationValue (
		class StaticEnvironment *a_static_environment,
		unsigned int a_index
	):
		static_environment (a_static_environment),
		index (a_index)
		{ };
public:
	string format () const;
public:
	shared_ptr <class Value> run
		(shared_ptr <class Value> a_continuation_paramter);
public:
	shared_ptr <class DynamicEnvironment> get_dynamic_environment_ptr ();
};


class TerminalContinuationValue: public Value {
	string format () const {
		return string {"TERMINAL_CONTINUATION"};
	};
};


class CapsuleValue: public Value {
public:
	shared_ptr <class Capsule> value;
public:
	CapsuleValue () { };
	CapsuleValue (shared_ptr <class Capsule> a_value): value (a_value) { };
public:
	string format () const {
		return string {"CAPSULE"};
	};
};


class PairValue: public Value {
public:
	shared_ptr <Value> key;
	shared_ptr <Value> value;
public:
	PairValue () { };
	PairValue (shared_ptr <Value> a_key,
		shared_ptr <Value> a_value):
		key (a_key),
		value (a_value)
		{ };
public:
	string format () const {
		return string {"("}
			+ key->format ()
			+ string {" ^pair "}
			+ value->format ()
			+ string {")"};
	};
};


class BrokenPairValue: public Value {
public:
	shared_ptr <Value> key;
public:
	BrokenPairValue () { };
	BrokenPairValue (shared_ptr <Value> a_key): key (a_key) { };
public:
	string format () const {
		return string {"(^broken-pair "}
			+ key->format ()
			+ string {")"};
	};
};


/* Issuer */


class Issuer {
public:
	unsigned int next_number;
public:
	Issuer (): next_number (0) { };
public:
	unsigned int operator () () {
		auto cache = next_number;
		next_number ++;
		return cache;
	};
};


/* Static Environment */


class StaticEnvironment {
public:
	class Program program;
	map <string, shared_ptr <class Primitive>> primitives;
	map <unsigned int, shared_ptr <class DynamicEnvironment>> continuations;
	Issuer issuer;
public:
	void initialize ();
	shared_ptr <class Value> run ();
	shared_ptr <class Value> run (
		class DynamicEnvironment a_env,
		shared_ptr <class Value> a_continuation_parameter
	);
};


/* Dynamic Environment */


class DynamicEnvironment {
public:
	class StaticEnvironment *static_environment;
public:
	unsigned int program_counter;
	shared_ptr <class Value> working_register;
	vector <map <string, shared_ptr <class Value>>> parameter_store_stack;
	vector <class ArgumentResolutionEnvironment> argument_resolution_environment_stack;
	vector <unsigned int> hierarchy_stack;
	vector <shared_ptr <class Value>> and_then_stack;
	shared_ptr <class Value> continuation_parameter;
	vector <shared_ptr <class DynamicEnvironment>> exception_stack;
public:
	void bootstrap ();
	string dump () const;
public:
	shared_ptr <class Value> run
		(shared_ptr <class Value> a_continuation_parameter);
};


class ArgumentResolutionEnvironment {
public:
	unsigned int start_point_of_argument_resolution_step;
	unsigned int end_point_of_argument_resolution;
	unsigned int current_directive_index;
	unsigned int go_if_all_directives_inactive;
	unsigned int anchor;
	map <string, shared_ptr <class Value>> current_parameters;
	map <string, shared_ptr <class Value>> next_parameters;
	shared_ptr <class ContinuationValue> continuation;
public:
	ArgumentResolutionEnvironment ():
		start_point_of_argument_resolution_step (0),
		end_point_of_argument_resolution (0),
		current_directive_index (0),
		go_if_all_directives_inactive (0),
		anchor (0)
		{ };
};


/* Primitive */


class Primitive {
public:
	class DynamicEnvironment *dynamic_environment;
	shared_ptr <class Value> return_value;
public:
	void initialize (class DynamicEnvironment *a_dynamic_environment);
	virtual void run () { };
	shared_ptr <class Value> get_parameter ();
	shared_ptr <class Value> get_parameter (string a_name);
	void set_return_value (shared_ptr <class Value> a_value);
	void throw_guest_catchable_exception ();
	void throw_guest_catchable_exception (shared_ptr <class Value> a_value);
};


map <string, shared_ptr <Primitive>> get_primitives ();


/* Comparer */


class Comparer {
public:
	shared_ptr <class DynamicEnvironment> continuation;
public:
	bool operator ()
		(const shared_ptr <class Value> & left, const shared_ptr <class Value> & right) const;
};


/* Capsule */


class Capsule {
public:
	class DynamicEnvironment *dynamic_environment;
	shared_ptr <class Value> return_value;
public:
	void initialize (class DynamicEnvironment *a_dynamic_environment);
	virtual void run () { };
	shared_ptr <class Value> get_parameter ();
	shared_ptr <class Value> get_parameter (string a_name);
	void set_return_value (shared_ptr <class Value> a_value);
	void throw_guest_catchable_exception ();
	void throw_guest_catchable_exception (shared_ptr <class Value> a_value);
};


class CapsuleAppendable: public Capsule {
public:
	virtual shared_ptr <CapsuleAppendable>
		append (shared_ptr <class Value> a_value)
	{
		return shared_ptr <CapsuleAppendable> {};
	};
};


class CapsuleArray: public CapsuleAppendable {
public:
	vector <shared_ptr <class Value>> container;
public:
	CapsuleArray () { };
	CapsuleArray (vector <shared_ptr <class Value>> a_container):
		container (a_container)
		{ };
public:
	void run ();
	shared_ptr <CapsuleAppendable>
		append (shared_ptr <class Value> a_value);
};


class CapsuleSet: public CapsuleAppendable {
public:
	Comparer comparer;
	shared_ptr <set <shared_ptr <class Value>, class Comparer>> container;
public:
	CapsuleSet () { };
	CapsuleSet (
		shared_ptr <set <shared_ptr <class Value>, class Comparer>> a_container,
		Comparer a_comparer
	):
		container (a_container),
		comparer (a_comparer)
	{ };
public:
	void run ();
	shared_ptr <CapsuleAppendable>
		append (shared_ptr <class Value> a_value);
};


class CapsuleMap: public CapsuleAppendable {
public:
	Comparer comparer;
	shared_ptr <map <
		shared_ptr <class Value>,
		shared_ptr <class Value>,
		class Comparer
	>> container;
public:
	CapsuleMap () { };
	CapsuleMap (
		shared_ptr <map <
			shared_ptr <class Value>,
			shared_ptr <class Value>,
			class Comparer
		>> a_container,
		Comparer a_comparer
	):
		container (a_container),
		comparer (a_comparer)
	{ };
public:
	void run ();
	shared_ptr <CapsuleAppendable>
		append (shared_ptr <class Value> a_value);
};


class CapsuleFile: public Capsule {
public:
	FILE * file;
public:
	CapsuleFile ():
		file (nullptr)
		{ };
	void run ();
};


class CapsuleTrivialContainer: public Capsule {
public:
	shared_ptr <class Value> value;
public:
	void run ();
};


}; /* namespace todes { */


#endif /* #ifndef TODES_H */

