trans;
	if: $action = main;
	parameters: [
		action: echo;
		main:
			[print; ^@, [
				action: [Map; ], (a ^pair #1), (b ^pair #2), (a ^pair #3);
				method: size;
			], ^!; ]
			> [print; ^@, [
				action: [Map; ], (a ^pair #1), (b ^pair #2), (a ^pair #3);
				method: at;
				main: a;
			], ^!; ];
	];

