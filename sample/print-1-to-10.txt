trans;
	if: $action = main;
	parameters: [
		print 1 to 10;
	];

trans;
	if: $action = print 1 to 10;
	parameters: [
		action: print 1 to 10 implementation;
		count: #1;
	];

trans;
	if: $action = print 1 to 10 implementation;
	parameters: [
		echo; ^false;
	];

trans;
	if: $action = print 1 to 10 implementation
		^and $count ^le #10;
	parameters: [
		count: $count + #1;
		do: [print; ^@, $count, ^!; ];
	];
