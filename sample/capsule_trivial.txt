trans;
	if: $action = main;
	parameters: [test trivial container; ];

trans;
	if: ($action = test trivial container) ^and (^not $step);
	parameters: [step: #10; ];

trans;
	if: ($action = test trivial container) ^and ($step = #10);
	parameters: [
		trivial container: [Trivial container; #1000; ];
		step: #20;
	];

trans;
	if: ($action = test trivial container) ^and ($step = #20);
	parameters: [
		do: [print; ^@, [$trivial container; get; false; ], ^!; ];
		step: #30;
	];

trans;
	if: ($action = test trivial container) ^and ($step = #30);
	parameters: [
		do: [$trivial container; set; #2000; ];
		step: #40;
	];

trans;
	if: ($action = test trivial container) ^and ($step = #40);
	parameters: [
		do: [print; ^@, [$trivial container; get; false; ], ^!; ];
		step: #50;
	];

trans;
	if: ($action = test trivial container) ^and ($step = #50);
	parameters: [echo; false; ];


