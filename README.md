# Todeslang Interpreter

See also:

* https://gitlab.com/hakabahitoyo/todeslang-spec
* https://gitlab.com/hakabahitoyo/todeslang-parser

## Install

```
sudo apt install build-essential
cd src
make clean
make
sudo make install
```

## Usage

```
todes input.cjson
```

## Try

```
cd sample
thel < hello.txt > hello.cjson
todes hello.cjson
```
